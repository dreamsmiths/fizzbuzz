# DREAMSMITHS APPLICANT TESTING

---

## Foreword

This test has been supplied in both JAVASCRIPT and PHP - please clone this repo and then complete the test in both languages. You will need the appropriate environments on your laptop to complete the tests. This is a test of skill and knowledge, while there is a solution and correct a output, our goal is to see what your level is in the use of these languages. This will provide us a primer to understand your capabilities. Failing or making mistakes will not necessarily disqualify your application for the position at DREAMSMITHS. We are looking at a variety of aspects in your code.

## Time

This test is not timed nor monitored. We are trusting you to take the test honestly and to not just find the answer online. Integrity, honesty and transparency is key to our culture and we would like to see it in our applicants. This test shouldn't take a senior developer more than 2 - 3 hours to complete. However if you are taking longer, do not be distressed, everyone works at their own pace and we understand this. Do your best and provide honest feedback.

## Environments

### JAVASCRIPT

- Can be completed in your browser (Chrome, Firefox, etc)
- We recommend you use VScode as your editor
- Run the index.html file and open your code inspector to view output

### PHP

- You will need to install a local PHP environment on your PC
- We recommend - Bitnami WAMP Stack (Windows) / MAMP (Mac)
- We recommend you use VScode as your editor (install an extension for PHP highlighting if necessary)
- Ensure your apache server is running
- Navigate to "//localhost/fizzbuzz/php/functions.php" in your browser to run
- Open conde inspector to view output

## References

You are allowed to use only the following two references in the test

- [https://developer.mozilla.org/en-US/](https://developer.mozilla.org/en-US/)
- [http://php.net/](http://php.net/)

## Submission

- Upload your code to new repo on your Github or Bitbucket account
- Please send us the link to the repo once complete

---

## Thank You and Good Luck

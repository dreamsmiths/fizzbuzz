<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PHP FizzBuzz</title>
  <link rel="stylesheet" href="../css/style.css">
</head>
<body>
  <!-- RESULTS WILL BE LOGGED TO CONSOLE -->
  <main>
    <header>
      <h2>PHP FizzBuzz Test</h2>
      <p>Results will be Logged to the Console</p>
      <hr>
    </header>
  </main>

  <?php
    // FOR CONSOLE LOGGING PURPOSES ONLY
    // DO NOT EDIT THIS CODE 
    // ------------------------------------ //
    function console_log( $data ){
      echo '<script>';
      echo 'console.log('. json_encode( $data ) .')';
      echo '</script>';
    }
    //------------------------------------- //

    // The Problem:
      // Write a function that prints the numbers from 1 to 100. 
      // For multiples of three print "Fizz" instead of the number 
      // For the multiples of five print "Buzz" instead of the number 
      // For numbers which are multiples of both three and five print "FizzBuzz"
      // Log your results to the console on the functions.php page 

    // Write your code below

    console_log('TEST');

  ?>
</body>
</html>